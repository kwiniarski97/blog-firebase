export const environment = {
    production: true,
    firebase: {
        apiKey: 'AIzaSyAvufOUdXZ_sJgN3ypTDaENPO9LDLXiKUI',
        authDomain: 'blog-app-2c022.firebaseapp.com',
        databaseURL: 'https://blog-app-2c022.firebaseio.com',
        projectId: 'blog-app-2c022',
        storageBucket: 'blog-app-2c022.appspot.com',
        messagingSenderId: '17219103756'
    },
    disqus: {
      shortname: 'ngx-ninja-cloud'
    }
};
