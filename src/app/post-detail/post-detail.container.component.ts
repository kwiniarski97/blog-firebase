import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostsQuery } from '../_state/posts.query';
import { PostsService } from '../_state/posts.service';
import { Observable } from 'rxjs';
import { Post } from '../shared/models/post.model';

@Component({
    template: `
        <div class="spinner" *ngIf="(isLoading$ | async)">
            <blog-spinner></blog-spinner>
        </div>
        <app-post-detail
            *ngIf="(post$ | async) as post"
            [post]="post"
        ></app-post-detail>
    `
})
export class PostDetailContainerComponent {
    post$: Observable<Post>;

    isLoading$: Observable<boolean>;

    private id: string;

    constructor(
        private route: ActivatedRoute,
        private postsQuery: PostsQuery,
        private postsService: PostsService
    ) {
        this.isLoading$ = this.postsQuery.selectLoading();
        this.id = this.route.snapshot.paramMap.get('id');

        this.fetchPost();
    }

    private fetchPost() {
        if (!this.postsQuery.hasEntity(this.id)) {
            this.postsService.getById(this.id);
        }
        this.post$ = this.postsQuery.getById(this.id);
    }
}
