import {
    Component,
    ChangeDetectionStrategy,
    AfterViewInit,
    Input,
    Output,
    EventEmitter,
    OnInit
} from '@angular/core';
import { Post } from '../shared/models/post.model';
import { TitleService } from '../core/title.service';

@Component({
    selector: 'app-post-detail',
    templateUrl: './post-detail.component.html',
    styleUrls: ['./post-detail.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostDetailComponent implements OnInit {
    @Input()
    post: Post;

    constructor(private title: TitleService) {}

    ngOnInit() {
        this.title.setTitle(this.post.title);
    }
}
