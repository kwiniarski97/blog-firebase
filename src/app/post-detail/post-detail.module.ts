import { NgModule } from '@angular/core';
import { DisqusModule } from 'ngx-disqus';
import { PostChipsComponent } from './post-chips/post-chips.component';
import { PostDetailComponent } from './post-detail.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { PostDetailContainerComponent } from './post-detail.container.component';
import { ChipComponent } from './post-chips/chip/chip.component';
import { environment } from 'src/environments/environment';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        DisqusModule.forRoot(environment.disqus.shortname)
    ],
    declarations: [
        PostDetailComponent,
        PostChipsComponent,
        PostDetailContainerComponent,
        ChipComponent
    ]
})
export class PostDetailModule {}
