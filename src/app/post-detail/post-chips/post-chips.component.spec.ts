import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostChipsComponent } from './post-chips.component';

describe('PostChipsComponent', () => {
  let component: PostChipsComponent;
  let fixture: ComponentFixture<PostChipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostChipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
