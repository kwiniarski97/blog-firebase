import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input
} from "@angular/core";
import { Tag } from "src/app/shared/models/tag.model";

@Component({
  selector: "blog-post-chips",
  templateUrl: "./post-chips.component.html",
  styleUrls: ["./post-chips.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostChipsComponent implements OnInit {
  @Input()
  tags: string[];

  colors: { background: string; text: string }[] = [
    { background: "#28359d", text: "#fff" },
    { background: "#ff4081", text: "#fff" },
    { background: "#e0e0e0", text: "rgba(0,0,0,.87)" }
  ];
  constructor() {}

  ngOnInit() {}

  trackFn(item: Tag, index) {
    return item.id;
  }
}
