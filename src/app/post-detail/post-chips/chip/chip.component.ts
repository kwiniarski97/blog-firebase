import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "blog-chip",
  templateUrl: "./chip.component.html",
  styleUrls: ["./chip.component.scss"]
})
export class ChipComponent implements OnInit {
  @Input()
  color: { background: string; text: string };
  constructor() {}

  ngOnInit() {}
}
