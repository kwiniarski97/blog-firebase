import { Injectable } from '@angular/core';
import { StoreConfig, EntityStore } from '@datorama/akita';
import { PostsState } from './posts.state';
import { Post } from 'src/app/shared/models/post.model';

const initialState: PostsState = {
    content: [],
    last: false,
    totalElements: 0,
    totalPages: 0,
    size: 0,
    number: 0,
    sort: null,
    first: true,
    numberOfElements: 0,
    searchValue: null
};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'posts' })
export class PostsStore extends EntityStore<PostsState, Post> {
    constructor() {
        super(initialState);
    }

    updatePage(page: { last: boolean; number: number }) {
        this.updateRoot(page);
    }

    setSearchValue(searchValue: string) {
        this.updateRoot({ searchValue: searchValue });
    }
}
