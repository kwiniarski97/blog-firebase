import { Injectable, OnDestroy } from '@angular/core';
import { takeUntil, map, timeout, finalize, flatMap } from 'rxjs/operators';
import { PostsStore } from './posts.store';
import { transaction } from '@datorama/akita';
import { Subject, from, TimeoutError } from 'rxjs';
import { Post } from 'src/app/shared/models/post.model';
import { AngularFirestore, QuerySnapshot } from '@angular/fire/firestore';
import { PaginatedResponse } from '../shared/models/paginated-reponse.model';
import { SnackBarService } from '../core/snack-bar.service';
import { BaseHttpService } from '../shared/base-http-service';

@Injectable({ providedIn: 'root' })
export class PostsService extends BaseHttpService implements OnDestroy {
    private destroy$ = new Subject<void>();

    private readonly PAGESIZE = 10;
    constructor(
        private postsStore: PostsStore,
        private db: AngularFirestore,
        protected snackBar: SnackBarService
    ) {
        super(snackBar);
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    getRecent(page: number) {
        this.postsStore.setLoading(true);
        from(
            this.db
                .collection<Post>('posts')
                .ref.orderBy('publishDate', 'desc')
                .where('publishDate', '<=', new Date())
                .get()
        )
            .pipe(
                takeUntil(this.destroy$),
                map(this.mapQuerySnapshot),
                timeout(30 * 1000),
                finalize(() => this.postsStore.setLoading(false))
            )
            .subscribe(
                posts => {
                    this.updatePosts({
                        content: posts,
                        first: page === 0,
                        last: posts.length < this.PAGESIZE,
                        number: page,
                        size: posts.length
                    });
                },
                (error: any) => {
                    if (error instanceof TimeoutError) {
                        super.displayTimeoutError();
                    } else {
                        super.displayGenericError();
                    }
                }
            );
    }

    getById(id: string) {
        this.postsStore.setLoading(true);
        this.db
            .doc(`posts/${id}`)
            .valueChanges()
            .pipe(takeUntil(this.destroy$))
            .subscribe((post: Post) => {
                post.id = id;
                this.updatePost(post);
            });
    }

    search(query: string, page: number = 0) {
        this.postsStore.setSearchValue(query);
        if (!query) {
            return;
        }

        const postsRef = this.db.collection('posts').ref;
        this.postsStore.setLoading(true);
        from(postsRef.where('tags', 'array-contains', query).get())
            .pipe(
                takeUntil(this.destroy$),
                map(this.mapQuerySnapshot)
            )
            .subscribe(posts => {
                this.updatePosts({
                    content: posts,
                    first: page === 0,
                    last: posts.length < this.PAGESIZE,
                    number: page,
                    size: posts.length
                });
            });
    }

    getAll(): void {
        this.postsStore.setLoading(true);
        this.db
            .collection('posts')
            .get()
            .pipe(
                takeUntil(this.destroy$),
                finalize(() => this.postsStore.setLoading(false)),
                map(this.mapQuerySnapshot)
            )
            .subscribe(posts => {
                this.postsStore.set(posts);
            });
    }

    create(post: Post): void {
        const ref = this.db.collection('posts').ref;
        this.postsStore.setLoading(true);
        from(ref.add({ ...post }))
            .pipe(
                flatMap(documentRef => documentRef.get()),
                takeUntil(this.destroy$),
                finalize(() => this.postsStore.setLoading(false))
            )
            .subscribe(
                document => {
                    const p = { id: document.id, ...document.data() } as Post;
                    this.postsStore.add(p);
                    this.snackbarService.alert(
                        `Succesfully added new post. id = ${p.id}`
                    );
                },
                error => {
                    this.snackbarService.alert('Error occured');
                }
            );
    }

    delete(id: string): void {
        const ref = this.db.collection('posts').ref;
        this.postsStore.setLoading(true);
        from(ref.doc(id).delete())
            .pipe(
                takeUntil(this.destroy$),
                finalize(() => this.postsStore.setLoading(false))
            )
            .subscribe(
                () => {
                    this.postsStore.remove(id);
                    this.snackbarService.alert('Successfully removed post');
                },
                error => {
                    this.snackbarService.alert('Error occured');
                }
            );
    }

    update(id: string, post: Post): void {
        const ref = this.db.collection('posts').ref;
        const doc = ref.doc(id);
        this.postsStore.setLoading(true);
        from(doc.update({ ...post }))
            .pipe(
                takeUntil(this.destroy$),
                finalize(() => this.postsStore.setLoading(false))
            )
            .subscribe(
                () => {
                    this.postsStore.upsert(id, post);
                    this.snackbarService.alert('Succesfully updated post.');
                },
                () => {
                    this.snackbarService.alert('Error occured');
                }
            );
    }

    // TODO: refractor
    private mapQuerySnapshot = (
        querySnapshot: firebase.firestore.QuerySnapshot
    ): Post[] =>
        querySnapshot.docs.map(doc => {
            return { id: doc.id, ...doc.data() } as Post;
        });

    @transaction()
    private updatePosts(res: PaginatedResponse<Post>) {
        const nextPage = res.number + 1;
        this.postsStore.add(res.content);
        this.postsStore.updatePage({ last: res.last, number: nextPage });
        this.postsStore.setLoading(false);
    }

    @transaction()
    private updatePost(res: Post) {
        this.postsStore.createOrReplace(res.id, res);
        this.postsStore.setLoading(false);
    }

    @transaction()
    public resetPaging() {
        this.postsStore.updatePage({ last: false, number: 0 });
    }
}
