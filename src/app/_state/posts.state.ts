import { EntityState } from "@datorama/akita";
import { PaginatedResponse } from "src/app/shared/models/paginated-reponse.model";
import { Post } from "src/app/shared/models/post.model";

export interface PostsState
  extends EntityState<Post>,
    PaginatedResponse<Post> {}
