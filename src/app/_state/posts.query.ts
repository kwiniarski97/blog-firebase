import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { PostsState } from "./posts.state";
import { PostsStore } from "./posts.store";
import { Post } from "src/app/shared/models/post.model";

@Injectable({ providedIn: "root" })
export class PostsQuery extends QueryEntity<PostsState, Post> {
  constructor(protected store: PostsStore) {
    super(store);
  }

  getLast() {
    return this.getSnapshot().last;
  }

  getPage() {
    return this.getSnapshot().number;
  }

  getById(id: string) {
    return this.selectEntity(id);
  }

  getSearchValue() {
    return this.getSnapshot().searchValue;
  }
}
