import { Component, NgZone } from "@angular/core";
import { environment } from "src/environments/environment";
import { akitaDevtools, enableAkitaProdMode } from "@datorama/akita";
import { UIStateService } from "./core/ui/_state/ui-state.service";
import { TitleService } from "./core/title.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  isDrawerOpen$ = this.uiStateService.isDrawerOpen$;
  constructor(
    private ngZone: NgZone,
    private uiStateService: UIStateService,
    private titleService: TitleService
  ) {
    if (!environment.production) {
      akitaDevtools(this.ngZone);
    } else {
      enableAkitaProdMode();
    }
    this.setUpDynamicTitle();
  }

  closeDrawer() {
    this.uiStateService.switchDrawer(false);
  }

  private setUpDynamicTitle() {
    this.titleService.listenToChanges();
  }
}
