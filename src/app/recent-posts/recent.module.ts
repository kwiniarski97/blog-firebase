import { NgModule } from "@angular/core";

import { RecentComponent } from "./recent.component";
import { SharedModule } from "../shared/shared.module";
import { PostListModule } from "../post-list/post-list.module";
import { CommonModule } from "@angular/common";

@NgModule({
  imports: [PostListModule, CommonModule],
  exports: [],
  declarations: [RecentComponent],
  providers: []
})
export class RecentModule {}
