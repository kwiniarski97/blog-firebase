import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Post } from "src/app/shared/models/post.model";
import { PostsQuery } from "../_state/posts.query";
import { PostsService } from "../_state/posts.service";

@Component({
  selector: "blog-recent",
  templateUrl: "./recent.component.html",
  styleUrls: ["./recent.component.scss"]
})
export class RecentComponent implements OnInit {
  posts$: Observable<Post[]>;
  isLoading$: Observable<boolean>;
  isLast$: Observable<boolean>;

  constructor(
    private postsQuery: PostsQuery,
    private postsService: PostsService
  ) {
    this.postsService.resetPaging();
  }

  ngOnInit() {
    this.fetchPosts();
    this.posts$ = this.postsQuery.selectAll();
    this.isLoading$ = this.postsQuery.selectLoading();
    this.isLast$ = this.postsQuery.select(s => s.last);
  }

  onScroll() {
    this.fetchPosts();
  }

  onMore() {
    this.fetchPosts();
  }

  trackPost(index, item) {
    return item.id;
  }

  private fetchPosts() {
    if (!this.postsQuery.getLast()) {
      this.postsService.getRecent(this.postsQuery.getPage());
    }
  }
}
