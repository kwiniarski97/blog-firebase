import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './ui/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HamburgerButtonComponent } from './ui/common/hamburger-button/hamburger-button.component';
import { DrawerComponent } from './ui/drawer/drawer.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { UIStateService } from './ui/_state/ui-state.service';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SnackBarService } from './snack-bar.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { TitleService } from './title.service';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        MatSidenavModule,
        RouterModule,
        MatListModule,
        MatSnackBarModule
    ],
    declarations: [
        HeaderComponent,
        HamburgerButtonComponent,
        DrawerComponent,
        NotFoundComponent
    ],
    exports: [HeaderComponent, DrawerComponent, MatSidenavModule],
    providers: [UIStateService, SnackBarService, TitleService]
})
export class CoreModule {}
