import {
  Component,
  OnInit,
  Renderer2,
  ViewChild,
  ElementRef,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { Subject } from "rxjs";

@Component({
  selector: "app-hamburger-button",
  templateUrl: "./hamburger-button.component.html",
  styleUrls: ["./hamburger-button.component.scss"]
})
export class HamburgerButtonComponent implements OnInit {
  @Input()
  drawerOpen: boolean;

  @Output()
  switch = new EventEmitter<void>();
  constructor() {}
  ngOnInit() {}

  switchDrawer() {
    this.switch.emit();
  }
}
