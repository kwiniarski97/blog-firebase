import { Component, OnInit } from "@angular/core";
import { UIStateService } from "../_state/ui-state.service";

@Component({
  selector: "app-drawer",
  templateUrl: "./drawer.component.html",
  styleUrls: ["./drawer.component.scss"]
})
export class DrawerComponent implements OnInit {
  isDrawerOpen$ = this.uiStateService.isDrawerOpen$;
  topLinks: Link[] = [
    new Link("Angular official site", "https://angular.io/"),
    new Link("Angular repository", "https://github.com/angular"),
    new Link("Angular documentation", "https://angular.io/docs")
  ];

  bottomLinks: Link[] = [
    new Link("Posts repository", "https://gitlab.com/kwiniarski97/blog-posts"),
    new Link("My projects", "https://gitlab.com/kwiniarski97"),
    new Link("Contact", "mailto: kwiniarski97@gmail.com")
  ];

  constructor(public uiStateService: UIStateService) {}

  ngOnInit() {}

  closeDrawer() {
    this.uiStateService.switchDrawer(false);
  }
}

class Link {
  label: string;
  href: string;

  constructor(label: string, routerLink: string) {
    this.label = label;
    this.href = routerLink;
  }
}
