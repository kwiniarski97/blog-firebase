import { Component, OnInit, OnDestroy, NgZone } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UIStateService } from "../_state/ui-state.service";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { Router } from "@angular/router";
import { BreakpointObserver, MediaMatcher } from "@angular/cdk/layout";
import { Platform } from "@angular/cdk/platform";
import { PostsService } from "src/app/_state/posts.service";

@Component({
  selector: "blog-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit, OnDestroy {
  searchForm: FormGroup;

  private _mobileSearchOpen = false;
  get mobileSearchOpen(): boolean {
    return this._mobileSearchOpen;
  }

  set mobileSearchOpen(value: boolean) {
    this._mobileSearchOpen = value;
  }

  destroy$ = new Subject();
  constructor(
    private formBuilder: FormBuilder,
    public uiStateService: UIStateService,
    private postService: PostsService,
    private router: Router,
    private ngZone: NgZone
  ) {
    this.searchForm = this.formBuilder.group({
      search: ["", [Validators.required]]
    });

    const breakpointObserver = new BreakpointObserver(
      new MediaMatcher(new Platform()),
      this.ngZone
    );
    // not mobile
    breakpointObserver
      .observe(["(min-width: 475px)"])
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result.matches) {
          this.mobileSearchOpen = true;
        }
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.destroy$.next();
  }

  switchDrawer() {
    this.uiStateService.switchDrawer();
  }

  search() {
    const query = this.searchForm.get("search").value.toUpperCase();
    this.postService.search(query);
    this.uiStateService.search(query);
    this.mobileSearchOpen = false;
    if (this.router.url !== "/search") {
      this.router.navigate(["/search"]);
    }
  }

  openSearchMobile() {
    this.mobileSearchOpen = !this.mobileSearchOpen;
  }
}
