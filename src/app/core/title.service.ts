import { Injectable, OnDestroy } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { takeUntil, filter, map, switchMap } from "rxjs/operators";
import { Subject } from "rxjs";
import { Title } from "@angular/platform-browser";
@Injectable()
export class TitleService implements OnDestroy {
  private destroy$ = new Subject<void>();
  private routerChanged$ = new Subject<void>();
  private readonly BASE_TITLE =
    "ngx-ninja.cloud - blog about programming. Javascript and stuff.";
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private title: Title
  ) {
    this.listenToChanges();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.routerChanged$.next();
    this.routerChanged$.complete();
  }

  listenToChanges(): void {
    this.router.events
      .pipe(
        takeUntil(this.destroy$),
        filter(navigation => navigation instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map(route => route.firstChild),
        switchMap(route => route.data),
        map(data => data["title"])
      )
      .subscribe((title: string) => {
        this.setTitle(title);
      });
  }

  setTitle(title: string) {
    this.title.setTitle(
      title ? `${title} | ${this.BASE_TITLE}` : this.BASE_TITLE
    );
  }
}
