import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { MatCardModule } from '@angular/material/card';
import { RecentModule } from './recent-posts/recent.module';
import { SearchResultsModule } from './search-results/search-results.module';
import { PostDetailModule } from './post-detail/post-detail.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

const angularModules = [BrowserModule, BrowserAnimationsModule];

const materialModules = [MatCardModule];

const myModules = [RecentModule, SearchResultsModule, PostDetailModule];

@NgModule({
    declarations: [AppComponent],
    imports: [
        ...angularModules,
        ...materialModules,
        ...myModules,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule.enablePersistence(),
        AppRoutingModule,
        CoreModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production
        })
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
