import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { SearchResultsComponent } from './search-results/search-results.component';
import { RecentComponent } from './recent-posts/recent.component';
import { NotFoundComponent } from './core/not-found/not-found.component';
import { PostDetailContainerComponent } from './post-detail/post-detail.container.component';

const routes: Routes = [
    {
        path: '',
        component: RecentComponent
    },
    {
        path: 'post/:id',
        component: PostDetailContainerComponent
    },
    {
        path: 'search',
        component: SearchResultsComponent,
        data: { title: 'Search results' }
    },
    {
        path: 'admin',
        loadChildren: './admin/admin.module#AdminModule'
    },
    {
        path: '**',
        component: NotFoundComponent,
        pathMatch: 'full',
        data: { title: '404' }
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
