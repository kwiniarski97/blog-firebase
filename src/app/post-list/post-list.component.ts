import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Post } from "src/app/shared/models/post.model";

@Component({
  selector: "blog-post-list",
  templateUrl: "./post-list.component.html",
  styleUrls: ["./post-list.component.scss"]
})
export class PostListComponent {
  @Input()
  posts: Post[];

  @Input()
  loading: boolean;

  @Output()
  scroll = new EventEmitter<void>();
  constructor() {}

  trackPost(item: Post, index: number) {
    return item.id;
  }

  onScroll() {
    this.scroll.emit();
  }
}
