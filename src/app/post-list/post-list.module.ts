import { NgModule } from "@angular/core";
import { PostListComponent } from "./post-list.component";
import { MatCardModule } from "@angular/material/card";
import { PostComponent } from "./post/post.component";
import { BlurImageDirective } from "./blur-image.directive";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { SharedModule } from "../shared/shared.module";
import { CoreModule } from "../core/core.module";

@NgModule({
  imports: [MatCardModule, InfiniteScrollModule, SharedModule],
  exports: [PostListComponent],
  declarations: [PostListComponent, PostComponent, BlurImageDirective],
  providers: []
})
export class PostListModule {}
