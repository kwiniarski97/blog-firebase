import { Directive, ElementRef, Renderer2 } from "@angular/core";

@Directive({
  selector: "[appBlurImage]"
})
export class BlurImageDirective {
  constructor(private ref: ElementRef, private renderer: Renderer2) {
    this.renderer.setStyle(this.ref.nativeElement, "filter", "url('#sharpBlur') brightness(30%)");
  }
}
