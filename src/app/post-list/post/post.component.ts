import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy
} from "@angular/core";
import { Router } from "@angular/router";
import { Post } from "src/app/shared/models/post.model";

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostComponent implements OnInit {
  @Input()
  post: Post;

  constructor(private router: Router) {}

  ngOnInit() {}

  goToDetails() {
    this.router.navigate(["post", this.post.id]);
  }
}
