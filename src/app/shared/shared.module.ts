import { NgModule } from '@angular/core';

// angular material
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { SpinnerComponent } from './spinner/spinner.component';
import { UnsanitizePipe } from './unsanitize.pipe';
import { CommonModule } from '@angular/common';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { TimestampPipe } from './timestamp.pipe';
import { AdsenseComponent } from './adsense/adsense.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MatCardModule } from '@angular/material/card';
import { PostDetailScreenComponent } from './post-detail-screen/post-detail-screen.component';

// /angular material

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        MatCardModule
    ],
    declarations: [
        SpinnerComponent,
        UnsanitizePipe,
        TimestampPipe,
        AdsenseComponent,
        PostDetailScreenComponent
    ],
    exports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        SpinnerComponent,
        UnsanitizePipe,
        MatListModule,
        TimestampPipe,
        AdsenseComponent,
        PostDetailScreenComponent
    ]
})
export class SharedModule {}
