export interface PaginatedResponse<T> {
  content: T[];
  last: boolean;
  size: number;
  number: number;
  first: boolean;
}
