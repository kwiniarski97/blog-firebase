import { ID } from "@datorama/akita";

export class Tag {
  id: number;
  name: string;
}

/**
 * A factory function that creates Tags
 */
export function createTag(params: Partial<Tag>) {
  return {} as Tag;
}
