export class Post {
    id: string;
    title: string;
    subtitle: string;
    mainImagePath: string;
    mainImageThumbnailPath: string;
    body: string;
    creationDate: Date | any;
    publishDate: Date | any;
    score: number;
    tags: string[];
}
