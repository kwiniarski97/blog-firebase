import { Component, Input, AfterViewInit } from "@angular/core";

@Component({
  selector: "blog-adsense",
  template: `
    <div>
      <ins
        class="adsbygoogle"
        style="display:block;"
        data-ad-client="ca-pub-4616971306891393"
        [attr.data-ad-slot]="data"
        data-ad-format="auto"
      ></ins>
    </div>
    <br />
  `
})
export class AdsenseComponent implements AfterViewInit {
  // TODO: add data add slot
  @Input() data;
  constructor() {}

  ngAfterViewInit() {
    setTimeout(() => {
      try {
        (window["adsbygoogle"] = window["adsbygoogle"] || []).push({});
      } catch (e) {
        console.error(e);
      }
    }, 2000);
  }
}
