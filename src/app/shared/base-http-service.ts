import { Inject, forwardRef } from '@angular/core';
import { SnackBarService } from '../core/snack-bar.service';

export abstract class BaseHttpService {
    constructor(protected snackbarService: SnackBarService) {}

    protected displayTimeoutError(): void {
        this.snackbarService.alert(
            'We could not connect to server.  Are you sure you got internet connection?'
        );
    }
    protected displayGenericError(): void {
        this.snackbarService.alert('Error occurred.');
    }
}
