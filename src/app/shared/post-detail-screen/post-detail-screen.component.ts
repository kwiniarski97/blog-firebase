import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input
} from '@angular/core';

@Component({
    selector: 'blog-post-detail-screen',
    templateUrl: './post-detail-screen.component.html',
    styleUrls: ['./post-detail-screen.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostDetailScreenComponent implements OnInit {
    @Input()
    text: string;
    constructor() {}

    ngOnInit() {}
}
