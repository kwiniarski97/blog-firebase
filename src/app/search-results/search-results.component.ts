import { Component, OnInit, OnDestroy, NgZone } from "@angular/core";
import { PostsService } from "../_state/posts.service";
import { PostsQuery } from "../_state/posts.query";
import { Observable, Subject } from "rxjs";
import { Post } from "src/app/shared/models/post.model";
import { takeUntil, filter, map } from "rxjs/operators";
import { Router } from "@angular/router";
import { UIStateService } from "../core/ui/_state/ui-state.service";

@Component({
  selector: "blog-search-results",
  templateUrl: "./search-results.component.html",
  styleUrls: ["./search-results.component.scss"]
})
export class SearchResultsComponent implements OnInit, OnDestroy {
  posts$: Observable<Post[]>;
  loading$: Observable<boolean>;
  destroy$ = new Subject<void>();
  searchQuery: string;
  constructor(
    private postsQuery: PostsQuery,
    private uiState: UIStateService,
    private router: Router
  ) {
    this.loading$ = this.postsQuery.selectLoading();
  }

  ngOnInit() {
    this.fetchPosts();
    this.watchForChanges();

    this.searchQuery = this.postsQuery.getSearchValue();
    this.checkIfQuerySet();
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  private watchForChanges() {
    this.uiState.searchEvent$
      .pipe(takeUntil(this.destroy$))
      .subscribe(query => {
        this.searchQuery = query;
        this.checkIfQuerySet();
        this.fetchPosts();
      });
  }

  private checkIfQuerySet() {
    if (this.searchQuery == null || this.searchQuery == "") {
      this.router.navigate(["/"]);
    }
  }

  private fetchPosts() {
    this.posts$ = this.postsQuery
      .selectAll()
      .pipe(map(p => p.filter(post => post.tags.includes(this.searchQuery))));
  }
}
