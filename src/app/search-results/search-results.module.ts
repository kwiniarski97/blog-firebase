import { NgModule } from "@angular/core";
import { SearchResultsComponent } from "./search-results.component";
import { PostListModule } from "../post-list/post-list.module";
import { CommonModule } from "@angular/common";

@NgModule({
  imports: [PostListModule, CommonModule],
  exports: [],
  declarations: [SearchResultsComponent],
  providers: []
})
export class SearchResultsModule {}
