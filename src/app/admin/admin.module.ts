import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AuthModule } from './auth/auth.module';
import { LoginComponent } from './auth/login/login.component';
import { AuthQuery } from './auth/_state/auth.query';
import { AuthService } from './auth/_state/auth.service';
import { AuthStore } from './auth/_state/auth.store';
import { AdminComponent } from './admin.component';
import { NavigationComponent } from './navigation/navigation.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { AdminOnlyGuard } from './admin-only.guard';
import { PostManagmentModule } from './post-managment/post-managment.module';
import { AdminPostListContainerComponent } from './post-managment/admin-post-list/admin-post-list.container.component';
import { PostCreateComponent } from './post-managment/post-create/post-create.component';
import { PostEditComponent } from './post-managment/post-edit/post-edit.component';
import { AdminPostsContainerComponent } from './post-managment/post-container.component';
import { MatListModule } from '@angular/material/list';
import { PostDetailModule } from '../post-detail/post-detail.module';
import { SharedModule } from '../shared/shared.module';

export const routes: Routes = [
    { path: 'login', component: LoginComponent },
    {
        path: '',
        component: AdminComponent,
        canActivate: [AdminOnlyGuard],
        children: [
            { path: '', redirectTo: '/admin/posts', pathMatch: 'full' },
            {
                path: 'posts',
                component: AdminPostsContainerComponent,
                children: [
                    {
                        path: '',
                        component: AdminPostListContainerComponent,
                        data: { title: 'Post list' }
                    },
                    {
                        path: 'create',
                        component: PostCreateComponent,
                        data: { title: 'New post' }
                    },
                    {
                        path: 'edit/:id',
                        component: PostEditComponent,
                        data: { title: 'Editing post' }
                    }
                ]
            }
        ]
    }
];

@NgModule({
    declarations: [AdminComponent, NavigationComponent],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        SharedModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireAuthModule,
        AuthModule,
        MatExpansionModule,
        PostManagmentModule,
        MatListModule
    ],
    providers: [AuthQuery, AuthService, AuthStore, AdminOnlyGuard]
})
export class AdminModule {}
