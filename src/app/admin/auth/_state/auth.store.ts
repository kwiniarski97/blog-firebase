import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { auth } from 'firebase';

@Injectable()
@StoreConfig({ name: 'auth' })
export class AuthStore extends Store<auth.UserCredential> {
    constructor() {
        super({});
    }

    setNewCredentials(credentials: auth.UserCredential) {
        this.update(credentials);
    }
}
