import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { AuthStore } from './auth.store';
import { auth } from 'firebase/app';

@Injectable()
export class AuthQuery extends Query<auth.UserCredential> {
    constructor(protected store: AuthStore) {
        super(store);
    }

    getAuth() {
        return this.selectOnce(a => auth);
    }
}
