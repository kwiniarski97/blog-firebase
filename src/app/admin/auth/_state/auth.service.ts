import { Injectable, OnDestroy } from '@angular/core';
import { AuthStore } from './auth.store';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { from, Subject } from 'rxjs';
import { takeUntil, finalize, flatMap } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { SnackBarService } from 'src/app/core/snack-bar.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthService implements OnDestroy {
    private destroy$ = new Subject<void>();
    constructor(
        private authStore: AuthStore,
        public afAuth: AngularFireAuth,
        private db: AngularFirestore,
        private snackBar: SnackBarService,
        private router: Router
    ) {}

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    signInWithGoogle(): void {
        const provider = new auth.GoogleAuthProvider();
        provider.addScope('profile');
        provider.addScope('email');
        this.authStore.setLoading(true);
        let creds: auth.UserCredential;

        from(this.afAuth.auth.signInWithPopup(provider))
            .pipe(
                flatMap(creditentials => {
                    creds = creditentials;
                    this.authStore.setLoading(true);
                    return from(
                        this.db.doc(`admins/${creditentials.user.uid}`).get()
                    ).pipe(
                        takeUntil(this.destroy$),
                        finalize(() => this.authStore.setLoading(false))
                    );
                }),
                takeUntil(this.destroy$),
                finalize(() => this.authStore.setLoading(false))
            )
            .subscribe(document => {
                if (document.exists) {
                    this.authStore.setNewCredentials(
                        JSON.parse(JSON.stringify(creds))
                    );
                    this.snackBar.alert(
                        `Successfully logged as ${document.data().email}`
                    );
                    this.router.navigate(['/admin']);
                } else {
                    this.snackBar.alert('You are not an admin');
                }
            });
    }
}
