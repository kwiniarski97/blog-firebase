import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../_state/auth.service';
import { AuthQuery } from '../_state/auth.query';
import { Router } from '@angular/router';

@Component({
    selector: 'blog-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    isLoading$: Observable<boolean>;

    constructor(
        private authService: AuthService,
        private authQuery: AuthQuery
    ) {
        this.isLoading$ = this.authQuery.selectLoading();
    }

    ngOnInit() {}

    login(): void {
        this.authService.signInWithGoogle();
    }
}
