import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    CanLoad,
    UrlSegment,
    Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { Route } from '@angular/compiler/src/core';
import { AuthQuery } from './auth/_state/auth.query';

@Injectable()
export class AdminOnlyGuard implements CanActivate, CanLoad {
    constructor(private authQuery: AuthQuery, private router: Router) {}
    canActivate(): boolean | Observable<boolean> | Promise<boolean> {
        return this.check();
    }
    canLoad(
        route: Route,
        segments: UrlSegment[]
    ): boolean | Observable<boolean> | Promise<boolean> {
        return this.check();
    }

    private check(): boolean | Observable<boolean> | Promise<boolean> {
        if (this.isLogged()) {
            return true;
        } else {
            this.router.navigate(['/admin', 'login']);
            return false;
        }
    }
    private isLogged(): boolean {
        const creditentials = this.authQuery.getSnapshot();
        return creditentials.credential != null;
    }
}
