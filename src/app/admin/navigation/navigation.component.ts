import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'blog-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
    tagRoutes: Route[] = [];
    postRoutes: Route[] = [
        {
            label: 'List',
            route: ['/admin']
        },
        {
            label: 'Create',
            route: ['/admin', 'posts', 'create']
        }
    ];
    adminsRoutes: Route[] = [];

    constructor() {}

    ngOnInit() {}
}

interface Route {
    label: string;
    route: string[];
}
