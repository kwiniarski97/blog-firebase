import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/shared/models/post.model';
import { PostsService } from 'src/app/_state/posts.service';

@Component({
    template: `
        <blog-post-form (onsubmit)="save($event)"></blog-post-form>
    `
})
export class PostCreateComponent implements OnInit {
    constructor(private postService: PostsService) {}

    ngOnInit() {}

    save(post: Post) {
        this.postService.create(post);
    }
}
