import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from 'src/app/_state/posts.service';
import { PostsQuery } from 'src/app/_state/posts.query';
import { Observable } from 'rxjs';
import { Post } from 'src/app/shared/models/post.model';

@Component({
    template: `
        <span *ngIf="(post$ | async) as post; else loading">
            <blog-post-form
                [post]="post"
                (onsubmit)="edit($event)"
            ></blog-post-form>
        </span>
        <ng-template #loading><blog-spinner></blog-spinner></ng-template>
    `
})
export class PostEditComponent implements OnInit {
    id: string;

    post$: Observable<Post>;
    constructor(
        private route: ActivatedRoute,
        private postService: PostsService,
        private postQuery: PostsQuery
    ) {
        this.id = route.snapshot.paramMap.get('id');
        this.fetchPost();
    }

    ngOnInit() {}

    edit(post: Post) {
        this.postService.update(this.id, post);
    }

    private fetchPost() {
        this.postService.getById(this.id);
        this.post$ = this.postQuery.selectEntity(this.id);
    }
}
