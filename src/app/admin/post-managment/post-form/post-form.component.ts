import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    AfterViewInit,
    ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MarkupConverterService } from './markup-converter.service';
import { Post } from 'src/app/shared/models/post.model';
import * as moment from 'moment';
import { post } from 'selenium-webdriver/http';

@Component({
    selector: 'blog-post-form',
    templateUrl: './post-form.component.html',
    styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements AfterViewInit {
    @Input()
    post: Post;

    @Output()
    onsubmit = new EventEmitter<Post>();
    form: FormGroup;
    content$: Observable<string>;

    // TODO: GET IT FROM FIREBASE
    availableTags: string[] = ['ANGULAR', 'VUE', 'JAVASCRIPT'];
    constructor(
        private fb: FormBuilder,
        private markupConverter: MarkupConverterService,
        private cd: ChangeDetectorRef
    ) {
        this.form = this.fb.group({
            title: '',
            subtitle: '',
            mainImagePath: '',
            mainImageThumbnailPath: '',
            publishDate: moment(),
            tags: '',
            body: '',
            score: 0
        });
        this.content$ = this.form
            .get('body')
            .valueChanges.pipe(
                map((markup: string) =>
                    this.markupConverter.convertMarkupToHtml(markup)
                )
            );
    }

    submit() {
        if (this.form.valid) {
            const values = this.form.value;
            const p = this.createPost(values);
            this.onsubmit.emit(p);
        }
    }

    private createPost(values: any): Post {
        const p = new Post();
        p.body = this.markupConverter.convertMarkupToHtml(values.body);
        p.creationDate = new Date();
        p.mainImagePath = values.mainImagePath;
        p.mainImageThumbnailPath = values.mainImageThumbnailPath;
        p.score = values.score;
        p.subtitle = values.subtitle;
        p.title = values.title;
        p.tags = values.tags;
        p.publishDate = values.publishDate.toDate();
        return p;
    }

    ngAfterViewInit() {
        // edit mode
        if (this.post) {
            this.fillFields();
        }
    }

    private fillFields() {
        this.form.patchValue({
            title: this.post.title,
            subtitle: this.post.subtitle,
            mainImagePath: this.post.mainImagePath,
            mainImageThumbnailPath: this.post.mainImageThumbnailPath,
            publishDate: moment.unix(this.post.publishDate.seconds),
            tags: this.post.tags,
            body: this.markupConverter.convertHtmlToMarkup(this.post.body),
            score: this.post.score
        });
        this.cd.detectChanges();
    }
}
