import { Injectable } from '@angular/core';
import * as showdown from 'showdown';
import * as hglt from 'showdown-highlight';

@Injectable()
export class MarkupConverterService {
    private converter: showdown.Converter;
    constructor() {
        this.converter = new showdown.Converter({
            extensions: [hglt]
        });
        this.converter.setFlavor('github');
    }

    public convertMarkupToHtml(markup: string): string {
        return this.converter.makeHtml(markup);
    }

    /**
     * might be defective
     */
    public convertHtmlToMarkup(html: string): string {
        return this.converter.makeMarkdown(html);
    }
}
