import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPostListComponent } from './admin-post-list/admin-post-list.component';
import { MatListModule } from '@angular/material/list';
import { AdminPostListContainerComponent } from './admin-post-list/admin-post-list.container.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PostFormComponent } from './post-form/post-form.component';
import { PostCreateComponent } from './post-create/post-create.component';
import { PostEditComponent } from './post-edit/post-edit.component';
import { AdminPostsContainerComponent } from './post-container.component';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MarkupConverterService } from './post-form/markup-converter.service';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
    declarations: [
        AdminPostListComponent,
        AdminPostListContainerComponent,
        PostFormComponent,
        PostCreateComponent,
        PostEditComponent,
        AdminPostsContainerComponent
    ],
    imports: [
        CommonModule,
        MatListModule,
        SharedModule,
        MatCardModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatSelectModule,
        MatMenuModule
    ],
    providers: [MarkupConverterService]
})
export class PostManagmentModule {}
