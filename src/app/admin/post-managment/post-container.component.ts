import { Component, OnInit } from '@angular/core';

@Component({
    template: `
        <mat-card><router-outlet></router-outlet></mat-card>
    `
})
export class AdminPostsContainerComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}
