import { Component, OnInit } from '@angular/core';
import { PostsService } from 'src/app/_state/posts.service';
import { PostsQuery } from 'src/app/_state/posts.query';
import { Observable } from 'rxjs';
import { Post } from 'src/app/shared/models/post.model';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
    template: `
        <blog-admin-post-list
            [posts]="posts$ | async"
            [isLoading]="isLoading$ | async"
            (edited)="edit($event)"
            (deleted)="delete($event)"
        ></blog-admin-post-list>
    `
})
export class AdminPostListContainerComponent implements OnInit {
    isLoading$: Observable<boolean>;
    posts$: Observable<Post[]>;
    constructor(
        private postService: PostsService,
        private postQuery: PostsQuery,
        private router: Router
    ) {
        this.isLoading$ = this.postQuery.selectLoading();
        this.fetchPosts();
    }

    ngOnInit() {}

    edit(id: string): void {
        this.router.navigate(['/admin', 'posts', 'edit', id]);
    }
    delete(id: string): void {
        if (confirm('Are you sure you want to delete this post?')) {
            if (confirm('Are you really sure? This is irreversible.')) {
                this.postService.delete(id);
            }
        }
    }

    private fetchPosts(): void {
        this.postService.getAll();
        this.posts$ = this.postQuery.selectAll();
    }
}
