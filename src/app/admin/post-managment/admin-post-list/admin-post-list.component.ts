import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from 'src/app/shared/models/post.model';

@Component({
    selector: 'blog-admin-post-list',
    templateUrl: './admin-post-list.component.html',
    styleUrls: ['./admin-post-list.component.scss']
})
export class AdminPostListComponent implements OnInit {
    @Input()
    posts: Post[];
    @Input()
    isLoading: boolean;
    @Output()
    edited = new EventEmitter<string>();
    @Output()
    deleted = new EventEmitter<string>();

    constructor() {}

    ngOnInit() {}

    edit(id: string): void {
        this.edited.emit(id);
    }

    delete(id: string): void {
        this.deleted.emit(id);
    }
}
